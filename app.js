/**
 * Задание 8 - Напишите эхо-сервер, который на запрос POST /echo будет возвращать тело запроса.
 * Напишите функцию которая посылает запрос на http://localhost:3000 POST /echo со следующей
 * полезной нагрузкой:
 * { message: "Привет сервис, я жду от тебя ответа"}
 * Примите ответ от сервера и выведите результат в консоль, используя синтаксис async/await.
 */

const express = require('express')
const cors = require('cors')
const bodyParser = require('body-parser');
const app = express()
const port = 3000

// json is not used in demo
// app.use(express.json())
app.use(cors())
// bodyParser is needed to add plain text content type handling
app.use(bodyParser.text())

app.post('/echo', (req, res) => {
    return res.send(req.body)
})

app.listen(port, () => {
    console.log(`Example app listening on port ${port}`)
})