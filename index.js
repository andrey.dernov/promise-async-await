/**
 * Задание 1 - Имитируем работу с сервером - Promise
 *
 * 1. Написать функцию getList, которая возвращает Promise с данными о списке задач, иммитируя
 * задержку перед получением в 2 секунды:
 * [
 * { id: 1, title: 'Task 1', isDone: false },
 * { id: 2, title: 'Task 2', isDone: true },
 * ]
 * 2. Написать скрипт который получит данные из функции getList и выведет на экран список задач.
 * 3. Изменить промис так, чтобы он возвращал ошибку
 * 4. Дополнить скрипт так, чтобы если промис возвращает ошибку выводилось сообщение об ошибке
 */

/**
 *
 * @returns {Promise<string>}
 */
function getList() {
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            resolve(
                [
                    {id: 1, title: 'Task 1', isDone: false},
                    {id: 2, title: 'Task 2', isDone: true},
                ]
            )
        }, 2000);
    })
}

getList().then(result => {
    console.log(result);
});

/**
 *
 * @returns {Promise<string>|Error}
 */
function getListRaisingError() {
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            reject('Unexpected error');
        }, 2000);
    })
}

getListRaisingError().then(result => {
    console.log(result);
}).catch(error => {
    console.log('Error happened: ', error)
})


/**
 * Задание 2 - Чейнинг (цепочки) промисов
 *
 * Написать функцию которая будет соберет строку "Я использую цепочки обещаний", конкотенируя каждое
 * слово через отдельный then блок с задержкой в 1 секунду на каждой итерации.
 * Результат вывести в консоль.
 */
/**
 *
 * @returns {Promise<string>}
 */
function chainedMessagesPromise() {
    return new Promise(resolve => {
        setTimeout(() => {
            resolve('Я')
        }, 1000)
    }).then(res => {
        return new Promise(resolve => {
            setTimeout(() => {
                resolve(res += ' использую')
            }, 1000)
        })
    }).then(res => {
        return new Promise(resolve => {
            setTimeout(() => {
                resolve(res += ' цепочки')
            }, 1000)
        })
    }).then(res => {
        return new Promise(resolve => {
            setTimeout(() => {
                resolve(res += ' обещаний')
            }, 1000)
        })
    });
}

console.log(chainedMessagesPromise().then(message => {
    console.log(message);
}))

/**
 *
 * @param {string[]} messages
 * @returns {Promise<string>}
 * @private
 */
function messagesPromiseChain(messages) {
    let stringPromise = Promise.resolve('');
    messages.forEach(message => {
            stringPromise = stringPromise.then(result => {
                    return new Promise(resolve => {
                        setTimeout(() => {
                            resolve(`${result} ${message}`)
                        }, 1000)
                    })
                }
            )
        }
    );
    return stringPromise;
}

const res = messagesPromiseChain(['Я', 'использую', 'цепочки', 'обещаний']);
res.then(result => console.log(result));

/**
 * Задание 3 - Параллельные обещания
 *
 * Написать функцию которая будет соберет строку "Я использую вызов обещаний параллельно",
 * используя функцию Promise.all(). Укажите следующее время задержки для каждого
 * промиса возвращаего слова:
 * Я - 1000,
 * использую - 800
 * вызов - 1200
 * обещаний - 700
 * параллельно - 500
 * Результат вывести в консоль.
 */
/**
 *
 * @returns {Promise<Awaited<Promise<string>>[]>}
 */
function getParallelExecPromiseArray() {
    return Promise.all([
        new Promise(res => setTimeout(() => res('Я'), 1000)),
        new Promise(res => setTimeout(() => res('использую'), 800)),
        new Promise(res => setTimeout(() => res('вызов'), 1200)),
        new Promise(res => setTimeout(() => res('обещаний'), 700)),
        new Promise(res => setTimeout(() => res('параллельно'), 500))
    ]);
}

getParallelExecPromiseArray().then(messages => {
    console.log('Результат параллельного выполнения Promise:')
    console.log(messages.join(' '))
})

/**
 * Задание 4 - Напишите функцию delay(ms), которая возвращает промис,
 * переходящий в состояние "resolved" через ms миллисекунд.
 *
 * delay(2000).then(() => console.log('Это сообщение вывелось через 2 секунды'))
 */
/**
 *
 * @param milliseconds
 * @returns {Promise<unknown>}
 */
function delay(milliseconds) {
    // return new Promise(resolve => setTimeout(() => resolve(), milliseconds));
    return new Promise((resolve) => setTimeout(resolve, milliseconds));
}

console.log('Запуск \'delay\'');
delay(2000).then(() => console.log('Это сообщение вывелось через 2 секунды'));

/**
 * Задание 5 - Решите 3 задачу, используя, функцию delay
 */
/**
 *
 * @returns {Promise<Awaited<Promise<string>>[]>}
 */
function getParallelExecPromiseArray2() {
    return Promise.all([
        new Promise(res => delay(1000).then(() => res('Я'))),
        new Promise(res => delay(800).then(() => res('использую'))),
        new Promise(res => delay(1200).then(() => res('вызов'))),
        new Promise(res => delay(700).then(() => res('обещаний'))),
        new Promise(res => delay(500).then(() => res('параллельно')))
    ]);
}

getParallelExecPromiseArray2().then(messages => {
    console.log('Результат параллельного выполнения Promise, использую функцию `delay`:')
    console.log(messages.join(' '))
})


/**
 * Задание 6 - Напишите функцию, которая загрузит данные по первому фильму в котором встретилась планета Татуин, используя
 * предоставленный API (https://swapi.dev)
 */
/**
 *
 * @param {string} planetName
 * @returns {Promise<string>}
 */
function getFilmName(planetName) {
    return fetch('https://swapi.dev/api/planets')
        .then(result => result.json())
        .then(planetsData => {
            const planetInfo = planetsData.results.find(planet => planet.name === planetName);

            if (!planetInfo) {
                throw new Error(`Planet with the name '${planetName}' not found.`)
            }

            return fetch(planetInfo.films[0]);
        }).then(filmResult => filmResult.json())
        .then(filmData => filmData.title)
        .catch(error => {
            console.log(error)
            return `Film not found. Cause: ${error.message}`;
        });
}

/**
 *
 * @param {string} planetName
 * @returns {Promise<string|string|string|*>}
 */
async function getFilmNameUsingAwait(planetName) {
    const data = await fetch('https://swapi.dev/api/films').then(res => res.json());
    const films = data.results;

    for (let i = 0; i < films.length; i++) {
        const planetsData = await Promise.all(
            films[i].planets.map(planetUrl => fetch(planetUrl).then(res => res.json()))
        );

        if (planetsData.find(planet => planet.name === planetName)) {
            return films[i].title;
        }
    }

    return 'Фильм не найден';
}

getFilmNameUsingAwait('Tatooine').then(filmName => console.log(filmName));
getFilmNameUsingAwait('Coruscant').then(filmName => console.log(filmName));
getFilmNameUsingAwait('Not exists').then(filmName => console.log(filmName));

getFilmName('Tatooine').then(filmName => console.log('Planet Tatooine. Film:', filmName));
getFilmName('Coruscant').then(filmName => console.log('Planet Coruscant. Film:', filmName));
getFilmName('Not exists').then(filmName => console.log('Planet `Not exists`. Film:', filmName));

/**
 * Задание 7 - Напишите функцию, которая выведет название транспортного средства на котором впервые ехал Anakin Skywalker, используя
 * предоставленный API (https://swapi.dev)
 */

/**
 *
 * @param {string} personName
 * @returns {Promise<string>}
 */
async function getVehicle(personName) {
    const person = await findEntity('https://swapi.dev/api/people/', person => person.name === personName);
    if (!person) return null;

    const vehicleUrl = person.vehicles.length > 0 ? person.vehicles[0] : null;
    if (!vehicleUrl) return null;

    const vehicle = await findEntity(vehicleUrl)

    return vehicle ? vehicle.name : null;
}

getVehicle('Anakin Skywalker').then(res => console.log(`Vehicle of the 'Anakin Skywalker' is: ${res}`));

/**
 *
 * @param {function|null} entitiesFindPredicate
 * @param {string} url
 * @returns {Promise<Object>}
 */
async function findEntity(url, entitiesFindPredicate = null) {
    let response = await fetch(url).then(res => res.json());

    if (!entitiesFindPredicate) return response;

    let entity = response?.results?.find(entitiesFindPredicate);
    while (!entity && response.next) {
        response = await fetch(response.next).then(res => res.json());
        entity = response.results?.find(entitiesFindPredicate);
    }
    return entity;
}

/**
 * Задание 8 - Напишите эхо-сервер, который на запрос POST /echo будет возвращать тело запроса.
 * Напишите функцию которая посылает запрос на http://localhost:3000 POST /echo со следующей
 * полезной нагрузкой:
 * { message: "Привет сервис, я жду от тебя ответа"}
 * Примите ответ от сервера и выведите результат в консоль, используя синтаксис async/await.
 */

//see also app.js

async function postEcho() {
    return fetch('http://localhost:3000/echo', {
        method: 'POST',
        headers: {
            'Content-Type': 'text/plain;charset=UTF-8'
        },
        body: 'Привет сервис, я жду от тебя ответа'
    }).then(r => r.text());
}

(async function () {
    const response = await postEcho();
    console.log('=================');
    console.log('Получен ответ от сервера: ', response);
    console.log('=================');
})();